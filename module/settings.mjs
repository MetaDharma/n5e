import { ModuleArtConfig } from "./module-art.mjs";

/**
 * Register all of the system's settings.
 */
export default function registerSystemSettings() {
  // Internal System Migration Version
  game.settings.register("n5e", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: false,
    type: String,
    default: ""
  });

  // Rest Recovery Rules
  game.settings.register("n5e", "restVariant", {
    name: "SETTINGS.N5eRestN",
    hint: "SETTINGS.N5eRestL",
    scope: "world",
    config: true,
    default: "normal",
    type: String,
    choices: {
      normal: "SETTINGS.N5eRestSHB",
      gritty: "SETTINGS.N5eRestGritty",
      epic: "SETTINGS.N5eRestEpic"
    }
  });

  // Diagonal Movement Rule
  game.settings.register("n5e", "diagonalMovement", {
    name: "SETTINGS.N5eDiagN",
    hint: "SETTINGS.N5eDiagL",
    scope: "world",
    config: true,
    default: "555",
    type: String,
    choices: {
      555: "SETTINGS.N5eDiagSHB",
      5105: "SETTINGS.N5eDiagKG",
      EUCL: "SETTINGS.N5eDiagEuclidean"
    },
    onChange: rule => canvas.grid.diagonalRule = rule
  });

  // Proficiency modifier type
  game.settings.register("n5e", "proficiencyModifier", {
    name: "SETTINGS.N5eProfN",
    hint: "SETTINGS.N5eProfL",
    scope: "world",
    config: true,
    default: "bonus",
    type: String,
    choices: {
      bonus: "SETTINGS.N5eProfBonus",
      dice: "SETTINGS.N5eProfDice"
    }
  });

  // Use Honor ability score
  game.settings.register("n5e", "honorScore", {
    name: "SETTINGS.N5eHonorN",
    hint: "SETTINGS.N5eHonorL",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    requiresReload: true
  });

  // Use Sanity ability score
  game.settings.register("n5e", "sanityScore", {
    name: "SETTINGS.N5eSanityN",
    hint: "SETTINGS.N5eSanityL",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    requiresReload: true
  });

  // Apply Dexterity as Initiative Tiebreaker
  game.settings.register("n5e", "initiativeDexTiebreaker", {
    name: "SETTINGS.N5eInitTBN",
    hint: "SETTINGS.N5eInitTBL",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  // Disable Experience Tracking
  game.settings.register("n5e", "disableExperienceTracking", {
    name: "SETTINGS.N5eNoExpN",
    hint: "SETTINGS.N5eNoExpL",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  // Disable Advancements
  game.settings.register("n5e", "disableAdvancements", {
    name: "SETTINGS.N5eNoAdvancementsN",
    hint: "SETTINGS.N5eNoAdvancementsL",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  // Collapse Item Cards (by default)
  game.settings.register("n5e", "autoCollapseItemCards", {
    name: "SETTINGS.N5eAutoCollapseCardN",
    hint: "SETTINGS.N5eAutoCollapseCardL",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: s => {
      ui.chat.render();
    }
  });

  // Allow Polymorphing
  game.settings.register("n5e", "allowPolymorphing", {
    name: "SETTINGS.N5eAllowPolymorphingN",
    hint: "SETTINGS.N5eAllowPolymorphingL",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  // Polymorph Settings
  game.settings.register("n5e", "polymorphSettings", {
    scope: "client",
    default: {
      keepPhysical: false,
      keepMental: false,
      keepSaves: false,
      keepSkills: false,
      mergeSaves: false,
      mergeSkills: false,
      keepClass: false,
      keepFeats: false,
      keepJutsu: false,
      keepItems: false,
      keepBio: false,
      keepVision: true,
      keepSelf: false,
      keepAE: false,
      keepOriginAE: true,
      keepOtherOriginAE: true,
      keepFeatAE: true,
      keepJutsuAE: true,
      keepEquipmentAE: true,
      keepClassAE: true,
      keepBackgroundAE: true,
      transformTokens: true
    }
  });

  // Critical Damage Modifiers
  game.settings.register("n5e", "criticalDamageModifiers", {
    name: "SETTINGS.N5eCriticalModifiersN",
    hint: "SETTINGS.N5eCriticalModifiersL",
    scope: "world",
    config: true,
    type: Boolean,
    default: false
  });

  // Critical Damage Maximize
  game.settings.register("n5e", "criticalDamageMaxDice", {
    name: "SETTINGS.N5eCriticalMaxDiceN",
    hint: "SETTINGS.N5eCriticalMaxDiceL",
    scope: "world",
    config: true,
    type: Boolean,
    default: false
  });

  // Strict validation
  game.settings.register("n5e", "strictValidation", {
    scope: "world",
    config: false,
    type: Boolean,
    default: true
  });

  // Dynamic art.
  game.settings.registerMenu("n5e", "moduleArtConfiguration", {
    name: "N5E.ModuleArtConfigN",
    label: "N5E.ModuleArtConfigL",
    hint: "N5E.ModuleArtConfigH",
    icon: "fa-solid fa-palette",
    type: ModuleArtConfig,
    restricted: true
  });

  game.settings.register("n5e", "moduleArtConfiguration", {
    name: "Module Art Configuration",
    scope: "world",
    config: false,
    type: Object,
    default: {
      n5e: {
        portraits: true,
        tokens: true
      }
    }
  });
}