import ActorSheetN5e from "./base-sheet.mjs";

/**
 * An Actor sheet for NPC type characters.
 */
export default class ActorSheetN5eNPC extends ActorSheetN5e {

  /** @inheritDoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["n5e", "sheet", "actor", "npc"],
      width: 750
    });
  }

  /* -------------------------------------------- */

  /** @override */
  static unsupportedItemTypes = new Set(["background", "clan", "class", "classmod", "subclass"]);

  /* -------------------------------------------- */
  /*  Context Preparation                         */
  /* -------------------------------------------- */

  /** @inheritDoc */
  async getData(options) {
    const context = await super.getData(options);

    // Challenge Rating
    const npcLevel = parseFloat(context.system.details.npcLevel ?? 0);
    const xpValue = parseFloat(context.system.details.xp.value ?? 0);

    return foundry.utils.mergeObject(context, {
      labels: {
        npcLevel: String(npcLevel),
        xpValue: String(xpValue + " XP"),
        type: this.actor.constructor.formatCreatureType(context.system.details.type),
        armorType: this.getArmorLabel()
      }
    });
  }

  /* -------------------------------------------- */

  /** @override */
  _prepareItems(context) {

    // Categorize Items as Features and Jutsu
    const features = {
      weapons: { label: game.i18n.localize("N5E.AttackPl"), items: [], hasActions: true,
        dataset: {type: "weapon", "weapon-type": "natural"} },
      actions: { label: game.i18n.localize("N5E.ActionPl"), items: [], hasActions: true,
        dataset: {type: "feat", "activation.type": "action"} },
      passive: { label: game.i18n.localize("N5E.Features"), items: [], dataset: {type: "feat"} },
      equipment: { label: game.i18n.localize("N5E.Inventory"), items: [], dataset: {type: "loot"}}
    };

    // Start by classifying items into groups for rendering
    let [jutsu, other] = context.items.reduce((arr, item) => {
      const {quantity, uses, recharge, target} = item.system;
      const ctx = context.itemContext[item.id] ??= {};
      ctx.isStack = Number.isNumeric(quantity) && (quantity !== 1);
      ctx.isExpanded = this._expanded.has(item.id);
      ctx.hasUses = uses && (uses.max > 0);
      ctx.isOnCooldown = recharge && !!recharge.value && (recharge.charged === false);
      ctx.isDepleted = item.isOnCooldown && (uses.per && (uses.value > 0));
      ctx.hasTarget = !!target && !(["none", ""].includes(target.type));
      ctx.canToggle = false;
      if ( item.type === "jutsu" ) arr[0].push(item);
      else arr[1].push(item);
      return arr;
    }, [[], []]);

    // Apply item filters
    jutsu = this._filterItems(jutsu, this._filters.jutsulist);
    other = this._filterItems(other, this._filters.features);

    // Organize Jutsulist
    const jutsulist = this._prepareJutsulist(context, jutsu);

    // Organize Features
    for ( let item of other ) {
      if ( item.type === "weapon" ) features.weapons.items.push(item);
      else if ( item.type === "feat" ) {
        if ( item.system.activation.type ) features.actions.items.push(item);
        else features.passive.items.push(item);
      }
      else features.equipment.items.push(item);
    }

    // Assign and return
    context.inventoryFilters = true;
    context.features = Object.values(features);
    context.jutsulist = jutsulist;
  }

  /* -------------------------------------------- */

  /**
   * Format NPC armor information into a localized string.
   * @returns {string}  Formatted armor label.
   */
  getArmorLabel() {
    const ac = this.actor.system.attributes.ac;
    const label = [];
    if ( ac.calc === "default" ) label.push(this.actor.armor?.name || game.i18n.localize("N5E.ArmorClassUnarmored"));
    else label.push(game.i18n.localize(CONFIG.N5E.armorClasses[ac.calc].label));
    return label.filterJoin(", ");
  }

  /* -------------------------------------------- */
  /*  Object Updates                              */
  /* -------------------------------------------- */

  /** @inheritDoc */
  async _updateObject(event, formData) {

    // Format NPC Challenge Rating
    const crs = {"1/8": 0.125, "1/4": 0.25, "1/2": 0.5};
    let crv = "system.details.npcLevel";
    let cr = formData[crv];
    cr = crs[cr] || parseFloat(cr);
    if ( cr ) formData[crv] = cr < 1 ? cr : parseInt(cr);

    // Parent ActorSheet update steps
    return super._updateObject(event, formData);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritDoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find(".tenacity .rollable").click(this._onRollTDFormula.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Roll a tenacity die
   * @param {string} [denomination]       The denomination to roll.
   * @param {object} options              Additional options which modify the roll.
   * @param {boolean} [options.chatMessage=true]  Display the chat message for this roll.
   * @returns {Promise<Roll>}                     The completed roll.
   */
  async _onRollTDFormula(denomination, chatMessage=true) {
      denomination = this.actor.system.attributes.td.denom;

      const td = this.actor.system.attributes.td;
      if ( td.value <= 0 ) {
        ui.notifications.error(game.i18n.format("N5E.TenacityDiceWarn", {name: this.actor.name}));
        return null;
      }

    // Prepare roll data
    const flavor = game.i18n.localize("N5E.TenacityDiceRoll");
    const rollData = {
      formula: `1${denomination}`,
      chatMessage
    };
    const messageData = {
      title: `${flavor}: ${this.actor.name}`,
      flavor,
      speaker: ChatMessage.getSpeaker({ actor: this }),
      "flags.n5e.roll": {type: "tenacityDice"}
    };
    
    /**
     * A hook event that fires before a tenacity die is rolled for an Actor.
     * @function n5e.preRollTenacityDice
     * @memberof hookEvents
     * @param {ActorN5e} actor              Actor for which the tenacity die is to be rolled.
     * @param {object} rollData             Configuration data for the pending roll.
     * @param {string} rollData.formula     Formula that will be rolled.
     * @param {object} messageData          Data used to create the chat message.
     * @param {string} denomination         Size of tenacity die to be rolled.
     */
    Hooks.callAll("n5e.preRollTenacityDice", this, rollData, denomination, messageData)

    const roll = new Roll(rollData.formula);
    await roll.evaluate({async: true});

    const updates = {
      actor: {"system.attributes.td.value": td.value - 1}
    };

    /**
     * A hook event that fires after a tenacity die has been rolled for an Actor, but before updates have been performed.
     * @function n5e.rollTenacityDice
     * @memberof hookEvents
     * @param {ActorN5e} actor   Actor for which the tenacity die has been rolled.
     * @param {Roll} roll        The resulting roll.
     * @param {object} updates
     * @param {object} updates.actor  Updates that will be applied to the actor.
     * @param {object} updates.class  Updates that will be applied to the class.
     * @returns {boolean}             Explicitly return `false` to prevent updates from being performed.
     */
    Hooks.call("n5e.rollTenacityDice", this, roll, updates);

    if ( rollData.chatMessage ) await roll.toMessage(messageData);

    // Perform updates
    this.actor.update({"system.attributes.td.value": td.value - 1});
    return roll;
  }
}