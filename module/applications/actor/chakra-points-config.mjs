import BaseConfigSheet from "./base-config.mjs";

/**
 * A form for configuring actor chakra points and bonuses.
 */
export default class ActorChakraPointsConfig extends BaseConfigSheet {
  constructor(...args) {
    super(...args);

    /**
     * Cloned copy of the actor for previewing changes.
     * @type {ActorN5e}
     */
    this.clone = this.object.clone();
  }

  /* -------------------------------------------- */

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["n5e", "actor-chakra-points-config"],
      template: "systems/n5e/templates/apps/chakra-points-config.hbs",
      width: 320,
      height: "auto",
      sheetConfig: false
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get title() {
    return `${game.i18n.localize("N5E.ChakraPointsConfig")}: ${this.document.name}`;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    return {
      cp: this.clone.system.attributes.cp,
      source: this.clone.toObject().system.attributes.cp,
      isCharacter: this.document.type === "character"
    };
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _getActorOverrides() {
    return Object.keys(foundry.utils.flattenObject(this.object.overrides?.system?.attributes || {}));
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    const cp = foundry.utils.expandObject(formData).cp;
    this.clone.updateSource({"system.attributes.cp": cp});
    const maxDelta = this.clone.system.attributes.cp.max - this.document.system.attributes.cp.max;
    cp.value = Math.max(this.document.system.attributes.cp.value + maxDelta, 0);
    return this.document.update({"system.attributes.cp": cp});
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritDoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find(".roll-chakra-points").click(this._onRollCPFormula.bind(this));
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onChangeInput(event) {
    await super._onChangeInput(event);
    const t = event.currentTarget;

    // Update clone with new data & re-render
    this.clone.updateSource({ [`system.attributes.${t.name}`]: t.value || null });
    if ( t.name !== "cp.formula" ) this.render();
  }

  /* -------------------------------------------- */

  /**
   * Handle rolling NPC health values using the provided formula.
   * @param {Event} event  The original click event.
   * @protected
   */
  async _onRollCPFormula(event) {
    event.preventDefault();
    try {
      const roll = await this.clone.rollNPCChakraPoints();
      this.clone.updateSource({"system.attributes.cp.max": roll.total});
      this.render();
    } catch(error) {
      ui.notifications.error(game.i18n.localize("N5E.CPFormulaError"));
      throw error;
    }
  }
}