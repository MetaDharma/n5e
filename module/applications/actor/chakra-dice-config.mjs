import BaseConfigSheet from "./base-config.mjs";

/**
 * A simple form to set actor chakra dice amounts.
 */
export default class ActorChakraDiceConfig extends BaseConfigSheet {

  /** @inheritDoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["n5e", "cd-config", "dialog"],
      template: "systems/n5e/templates/apps/chakra-dice-config.hbs",
      width: 360,
      height: "auto"
    });
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  get title() {
    return `${game.i18n.localize("N5E.ChakraDiceConfig")}: ${this.object.name}`;
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  getData(options) {
    return {
      classes: this.object.items.reduce((classes, item) => {
        if (item.type === "class") {
          classes.push({
            classItemId: item.id,
            name: item.name,
            diceDenom: item.system.chakraDice,
            currentChakraDice: item.system.levels - item.system.chakraDiceUsed,
            maxChakraDice: item.system.levels,
            canRoll: (item.system.levels - item.system.chakraDiceUsed) > 0
          });
        }
        return classes;
      }, []).sort((a, b) => parseInt(b.diceDenom.slice(1)) - parseInt(a.diceDenom.slice(1)))
    };
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  activateListeners(html) {
    super.activateListeners(html);

    // Hook up -/+ buttons to adjust the current value in the form
    html.find("button.increment,button.decrement").click(event => {
      const button = event.currentTarget;
      const current = button.parentElement.querySelector(".current");
      const max = button.parentElement.querySelector(".max");
      const direction = button.classList.contains("increment") ? 1 : -1;
      current.value = Math.clamped(parseInt(current.value) + direction, 0, parseInt(max.value));
    });

    html.find("button.roll-cd").click(this._onRollChakraDie.bind(this));
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  async _updateObject(event, formData) {
    const actorItems = this.object.items;
    const classUpdates = Object.entries(formData).map(([id, cd]) => ({
      _id: id,
      "system.chakraDiceUsed": actorItems.get(id).system.levels - cd
    }));
    return this.object.updateEmbeddedDocuments("Item", classUpdates);
  }

  /* -------------------------------------------- */

  /**
   * Rolls the chakra die corresponding with the class row containing the event's target button.
   * @param {MouseEvent} event Triggering click event.
   * @protected
   */
  async _onRollChakraDie(event) {
    event.preventDefault();
    const button = event.currentTarget;
    await this.object.rollChakraDie(button.dataset.cdDenom);

    // Re-render dialog to reflect changed chakra dice quantities
    this.render();
  }
}