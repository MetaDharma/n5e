import { FormulaField, MappingField } from "../../fields.mjs";
import CommonTemplate from "./common.mjs";

/**
 * @typedef {object} SkillData
 * @property {number} value            Proficiency level creature has in this skill.
 * @property {string} ability          Default ability used for this skill.
 * @property {object} bonuses          Bonuses for this skill.
 * @property {string} bonuses.check    Numeric or dice bonus to skill's check.
 * @property {string} bonuses.passive  Numeric bonus to skill's passive check.
 */

/**
 * A template for all actors that are creatures
 *
 * @property {object} bonuses
 * @property {AttackBonusesData} bonuses.mwak        Bonuses to melee weapon attacks.
 * @property {AttackBonusesData} bonuses.rwak        Bonuses to ranged weapon attacks.
 * @property {AttackBonusesData} bonuses.mgak        Bonuses to melee genjutsu attacks.
 * @property {AttackBonusesData} bonuses.rgak        Bonuses to ranged genjutsu attacks.
 * @property {AttackBonusesData} bonuses.mnak        Bonuses to melee ninjutsu attacks.
 * @property {AttackBonusesData} bonuses.rnak        Bonuses to ranged ninjutsu attacks.
 * @property {AttackBonusesData} bonuses.mtak        Bonuses to melee taijutsu attacks.
 * @property {AttackBonusesData} bonuses.rtak        Bonuses to ranged taijutsu attacks.
 * @property {object} bonuses.abilities              Bonuses to ability scores.
 * @property {string} bonuses.abilities.check        Numeric or dice bonus to ability checks.
 * @property {string} bonuses.abilities.save         Numeric or dice bonus to ability saves.
 * @property {string} bonuses.abilities.skill        Numeric or dice bonus to skill checks.
 * @property {object} bonuses.jutsu                  Bonuses to jutsu.
 * @property {string} bonuses.jutsu.dc               Numeric bonus to jutsucasting DC.
 * @property {Object<string, SkillData>} skills      Actor's skills.
 */
export default class CreatureTemplate extends CommonTemplate {
  static defineSchema() {
    return this.mergeSchema(super.defineSchema(), {
      bonuses: new foundry.data.fields.SchemaField({
        mwak: makeAttackBonuses({label: "N5E.BonusMWAttack"}),
        rwak: makeAttackBonuses({label: "N5E.BonusRWAttack"}),
        mgak: makeAttackBonuses({label: "N5E.BonusMGAttack"}),
        rgak: makeAttackBonuses({label: "N5E.BonusRGAttack"}),
        mnak: makeAttackBonuses({label: "N5E.BonusMNAttack"}),
        rnak: makeAttackBonuses({label: "N5E.BonusRNAttack"}),
        mtak: makeAttackBonuses({label: "N5E.BonusMTAttack"}),
        rtak: makeAttackBonuses({label: "N5E.BonusRTAttack"}),
        abilities: new foundry.data.fields.SchemaField({
          check: new FormulaField({required: true, label: "N5E.BonusAbilityCheck"}),
          save: new FormulaField({required: true, label: "N5E.BonusAbilitySave"}),
          skill: new FormulaField({required: true, label: "N5E.BonusAbilitySkill"})
        }, {label: "N5E.BonusAbility"}),
        jutsu: new foundry.data.fields.SchemaField({
          dc: new FormulaField({required: true, deterministic: true, label: "N5E.BonusJutsuDC"})
        }, {label: "N5E.BonusJutsu"})
      }, {label: "N5E.Bonuses"}),
      skills: new MappingField(new foundry.data.fields.SchemaField({
        value: new foundry.data.fields.NumberField({
          required: true, nullable: false, min: 0, max: 2, step: 0.5, initial: 0, label: "N5E.ProficiencyLevel"
        }),
        ability: new foundry.data.fields.StringField({required: true, initial: "dex", label: "N5E.Ability"}),
        bonuses: new foundry.data.fields.SchemaField({
          check: new FormulaField({required: true, label: "N5E.SkillBonusCheck"}),
          passive: new FormulaField({required: true, label: "N5E.SkillBonusPassive"})
        }, {label: "N5E.SkillBonuses"})
      }), {
        initialKeys: CONFIG.N5E.skills, initialValue: this._initialSkillValue,
        initialKeysOnly: true, label: "N5E.Skills"
      }),
      tools: new MappingField(new foundry.data.fields.SchemaField({
        value: new foundry.data.fields.NumberField({
          required: true, nullable: false, min: 0, max: 2, step: 0.5, initial: 1, label: "N5E.ProficiencyLevel"
        }),
        ability: new foundry.data.fields.StringField({required: true, initial: "int", label: "N5E.Ability"}),
        bonuses: new foundry.data.fields.SchemaField({
          check: new FormulaField({required: true, label: "N5E.CheckBonus"})
        }, {label: "N5E.ToolBonuses"})
      })),
      jutsu: new MappingField(new foundry.data.fields.SchemaField({
      }), {initialKeys: this._jutsuRanks, label: "N5E.JutsuRanks"})
    });
  }

  /* -------------------------------------------- */

  /**
   * Populate the proper initial abilities for the skills.
   * @param {string} key      Key for which the initial data will be created.
   * @param {object} initial  The initial skill object created by SkillData.
   * @returns {object}        Initial skills object with the ability defined.
   * @private
   */
  static _initialSkillValue(key, initial) {
    if ( CONFIG.N5E.skills[key]?.ability ) initial.ability = CONFIG.N5E.skills[key].ability;
    return initial;
  }

  /* -------------------------------------------- */

  /**
   * Helper for building the default list of jutsu ranks.
   * @type {string[]}
   * @private
   */
  static get _jutsuRanks() {
    const ranks = Object.keys(CONFIG.N5E.jutsuRanks).filter(a => a !== "0").map(r => `jutsu${r}`);
    return [...ranks];
  }

  /* -------------------------------------------- */
  /*  Migrations                                  */
  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    super.migrateData(source);
    CreatureTemplate.#migrateSensesData(source);
    CreatureTemplate.#migrateToolData(source);
  }

  /* -------------------------------------------- */

  /**
   * Migrate the actor traits.senses string to attributes.senses object.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateSensesData(source) {
    const original = source.traits?.senses;
    if ( (original === undefined) || (typeof original !== "string") ) return;
    source.attributes ??= {};
    source.attributes.senses ??= {};

    // Try to match old senses with the format like "Darkvision 60 ft, Blindsight 30 ft"
    const pattern = /([A-z]+)\s?([0-9]+)\s?([A-z]+)?/;
    let wasMatched = false;

    // Match each comma-separated term
    for ( let s of original.split(",") ) {
      s = s.trim();
      const match = s.match(pattern);
      if ( !match ) continue;
      const type = match[1].toLowerCase();
      if ( (type in CONFIG.N5E.senses) && !(type in source.attributes.senses) ) {
        source.attributes.senses[type] = Number(match[2]).toNearest(0.5);
        wasMatched = true;
      }
    }

    // If nothing was matched, but there was an old string - put the whole thing in "special"
    if ( !wasMatched && original ) source.attributes.senses.special = original;
  }

  /* -------------------------------------------- */

  /**
   * Migrate traits.toolProf to the tools field.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateToolData(source) {
    const original = source.traits?.toolProf;
    if ( !original || foundry.utils.isEmpty(original.value) ) return;
    source.tools ??= {};
    for ( const prof of original.value ) {
      const validProf = (prof in CONFIG.N5E.toolProficiencies) || (prof in CONFIG.N5E.toolIds);
      if ( !validProf || (prof in source.tools) ) continue;
      source.tools[prof] = {
        value: 1,
        ability: "int",
        bonuses: {check: ""}
      };
    }
  }
}

/* -------------------------------------------- */

/**
 * Data structure for actor's attack bonuses.
 *
 * @typedef {object} AttackBonusesData
 * @property {string} attack  Numeric or dice bonus to attack rolls.
 * @property {string} damage  Numeric or dice bonus to damage rolls.
 */

/**
 * Produce the schema field for a simple trait.
 * @param {object} schemaOptions  Options passed to the outer schema.
 * @returns {AttackBonusesData}
 */
function makeAttackBonuses(schemaOptions={}) {
  return new foundry.data.fields.SchemaField({
    attack: new FormulaField({required: true, label: "N5E.BonusAttack"}),
    damage: new FormulaField({required: true, label: "N5E.BonusDamage"})
  }, schemaOptions);
}