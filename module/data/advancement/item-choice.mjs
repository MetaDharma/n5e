import { MappingField } from "../fields.mjs";
import JutsuConfigurationData from "./jutsu-config.mjs";

export default class ItemChoiceConfigurationData extends foundry.abstract.DataModel {
  static defineSchema() {
    return {
      hint: new foundry.data.fields.StringField({label: "N5E.AdvancementHint"}),
      choices: new MappingField(new foundry.data.fields.NumberField(), {
        hint: "N5E.AdvancementItemChoiceLevelsHint"
      }),
      allowDrops: new foundry.data.fields.BooleanField({
        initial: true, label: "N5E.AdvancementConfigureAllowDrops",
        hint: "N5E.AdvancementConfigureAllowDropsHint"
      }),
      type: new foundry.data.fields.StringField({
        blank: false, nullable: true, initial: null,
        label: "N5E.AdvancementItemChoiceType", hint: "N5E.AdvancementItemChoiceTypeHint"
      }),
      pool: new foundry.data.fields.ArrayField(new foundry.data.fields.StringField(), {label: "DOCUMENT.Items"}),
      jutsu: new foundry.data.fields.EmbeddedDataField(JutsuConfigurationData, {nullable: true, initial: null}),
      restriction: new foundry.data.fields.SchemaField({
        type: new foundry.data.fields.StringField({label: "N5E.Type"}),
        subtype: new foundry.data.fields.StringField({label: "N5E.Subtype"}),
        rank: new foundry.data.fields.StringField({label: "N5E.JutsuRank"})
      })
    };
  }
}