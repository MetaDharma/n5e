/**
 * Data model template with information on physical items.
 *
 * @property {number} quantity            Number of items in a stack.
 * @property {number} bulk                Item's bulk.
 * @property {object} price
 * @property {number} price.value         Item's cost in ryo.
 * @property {string} price.denomination  Currency denomination used to determine price.
 * @property {string} rarity              Item rarity as defined in `N5E.itemRarity`.
 * @property {boolean} identified         Has this item been identified?
 * @mixin
 */
export default class PhysicalItemTemplate extends foundry.abstract.DataModel {
  /** @inheritdoc */
  static defineSchema() {
    return {
      quantity: new foundry.data.fields.NumberField({
        required: true, nullable: false, integer: true, initial: 1, min: 0, label: "N5E.Quantity"
      }),
      bulk: new foundry.data.fields.NumberField({
        required: true, nullable: false, initial: 0, min: 0, label: "N5E.Bulk"
      }),
      price: new foundry.data.fields.SchemaField({
        value: new foundry.data.fields.NumberField({
          required: true, nullable: false, initial: 0, min: 0, label: "N5E.Price"
        }),
        denomination: new foundry.data.fields.StringField({
          required: true, blank: false, initial: "ryo", label: "N5E.Currency"
        })
      }, {label: "N5E.Price"}),
      rarity: new foundry.data.fields.StringField({required: true, blank: true, label: "N5E.Rarity"}),
      identified: new foundry.data.fields.BooleanField({required: true, initial: true, label: "N5E.Identified"})
    };
  }

  /* -------------------------------------------- */
  /*  Migrations                                  */
  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    PhysicalItemTemplate.#migratePrice(source);
    PhysicalItemTemplate.#migrateRarity(source);
    PhysicalItemTemplate.#migrateBulk(source);
  }

  /* -------------------------------------------- */

  /**
   * Migrate the item's price from a single field to an object with currency.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migratePrice(source) {
    if ( !("price" in source) || foundry.utils.getType(source.price) === "Object" ) return;
    source.price = {
      value: Number.isNumeric(source.price) ? Number(source.price) : 0,
      denomination: "ryo"
    };
  }

  /* -------------------------------------------- */

  /**
   * Migrate the item's rarity from freeform string to enum value.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateRarity(source) {
    if ( !("rarity" in source) || CONFIG.N5E.itemRarity[source.rarity] ) return;
    source.rarity = Object.keys(CONFIG.N5E.itemRarity).find(key =>
      CONFIG.N5E.itemRarity[key].toLowerCase() === source.rarity.toLowerCase()
    ) ?? "";
  }

  /* -------------------------------------------- */

  /**
   * Convert null bulks to 0.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateBulk(source) {
    if ( !("bulk" in source) ) return;
    if ( (source.bulk === null) || (source.bulk === undefined) ) source.bulk = 0;
  }
}