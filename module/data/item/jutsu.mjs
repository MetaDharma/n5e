import SystemDataModel from "../abstract.mjs";
import { FormulaField, MappingField } from "../fields.mjs";
import ActionTemplate from "./templates/action.mjs";
import ActivatedEffectTemplate from "./templates/activated-effect.mjs";
import ItemDescriptionTemplate from "./templates/item-description.mjs";

/**
 * Data definition for Jutsu items.
 * @mixes ItemDescriptionTemplate
 * @mixes ActivatedEffectTemplate
 * @mixes ActionTemplate
 *
 * @property {number} rank                       Rank of the jutsu.
 * @property {number} baseRank                   Base rank of the jutsu.
 * @property {string} class                      Class to which this jutsu belongs.
 * @property {object} components                 General components for this jutsu.
 * @property {boolean} components.handSeals      Does this jutsu require hand seals?
 * @property {boolean} components.chakraMolding  Does this jutsu require chakra molding?
 * @property {boolean} components.chakraSeals    Does this jutsu require chakra seal?
 * @property {boolean} components.mobility       Does this jutsu require mobility?
 * @property {boolean} components.weapon         Does this jutsu require a weapon?
 * @property {boolean} components.ninjaTool      Does this jutsu require a ninja tool?
 * @property {boolean} components.concentration  Does this jutsu require concentration?
 * @property {object} keywords                   General keywords for this jutsu.
 * @property {object} materials                  Details on material components required for this jutsu.
 * @property {string} materials.value            Description of the material components required for casting.
 * @property {boolean} materials.consumed        Are these material components consumed during casting?
 * @property {number} materials.cost             Ryo cost for the required components.
 * @property {number} materials.supply           Quantity of this component available.
 * @property {object} scaling                    Details on how casting at higher ranks affects this jutsu.
 * @property {string} scaling.mode               Jutsu scaling mode as defined in `N5E.jutsuScalingModes`.
 * @property {string} scaling.formula            Dice formula used for scaling.
 */
export default class JutsuData extends SystemDataModel.mixin(
  ItemDescriptionTemplate, ActivatedEffectTemplate, ActionTemplate
) {
  /** @inheritdoc */
  static defineSchema() {
    return this.mergeSchema(super.defineSchema(), {
      rank: new foundry.data.fields.NumberField({
        required: true, integer: true, initial: 1, min: 0, label: "N5E.JutsuRank"
      }),
      baseRank: new foundry.data.fields.NumberField({
        required: true, integer: true, initial: 1, min: 0, label: "N5E.BaseRank"
      }),
      upcastCost: new foundry.data.fields.NumberField({
        required: true, integer: true, initial: 1, min: 0, label: "N5E.UpcastCost"
      }),
      class: new foundry.data.fields.StringField({required: true, label: "N5E.JutsuClass"}),
      components: new MappingField(new foundry.data.fields.BooleanField(), {
        required: true, label: "N5E.JutsuComponents",
        initialKeys: Object.keys(CONFIG.N5E.jutsuComponents)
      }),
      keywords: new MappingField(new foundry.data.fields.BooleanField(), {
        required: true, label: "N5E.JutsuKeywords",
        initialKeys: Object.keys(CONFIG.N5E.jutsuKeywords)
      }),
      materials: new foundry.data.fields.SchemaField({
        value: new foundry.data.fields.StringField({required: true, label: "N5E.JutsuMaterialsDescription"}),
        consumed: new foundry.data.fields.BooleanField({required: true, label: "N5E.JutsuMaterialsConsumed"}),
        cost: new foundry.data.fields.NumberField({
          required: true, initial: 0, min: 0, label: "N5E.JutsuMaterialsCost"
        }),
        supply: new foundry.data.fields.NumberField({
          required: true, initial: 0, min: 0, label: "N5E.JutsuMaterialsSupply"
        })
      }, {label: "N5E.JutsuMaterials"}),
      scaling: new foundry.data.fields.SchemaField({
        mode: new foundry.data.fields.StringField({required: true, initial: "none", label: "N5E.ScalingMode"}),
        formula: new FormulaField({required: true, nullable: true, initial: null, label: "N5E.ScalingFormula"})
      }, {label: "N5E.Scaling"})
    });
  }

  /* -------------------------------------------- */
  /*  Migrations                                  */
  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    super.migrateData(source);
    JutsuData.#migrateComponentData(source);
    JutsuData.#migrateKeywordData(source);
    JutsuData.#migrateScaling(source);
  }

  /* -------------------------------------------- */

  /**
   * Migrate the jutsu's component object to remove any old, non-boolean values.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateComponentData(source) {
    if ( !source.components ) return;
    for ( const [key, value] of Object.entries(source.components) ) {
      if ( typeof value !== "boolean" ) delete source.components[key];
    }
  }

  /* -------------------------------------------- */

  /**
   * Migrate the jutsu's keyword object to remove any old, non-boolean values.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateKeywordData(source) {
    if ( !source.keywords ) return;
    for ( const [key, value] of Object.entries(source.keywords) ) {
      if ( typeof value !== "boolean" ) delete source.keywords[key];
    }
  }

  /* -------------------------------------------- */

  /**
   * Migrate jutsu scaling.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateScaling(source) {
    if ( !("scaling" in source) ) return;
    if ( (source.scaling.mode === "") || (source.scaling.mode === null) ) source.scaling.mode = "none";
  }

  /* -------------------------------------------- */
  /*  Getters                                     */
  /* -------------------------------------------- */

  /**
   * Properties displayed in chat.
   * @type {string[]}
   */
  get chatProperties() {
    return [
      this.parent.labels.rank,
      this.parent.labels.components.m + (this.parent.labels.materials ? ` (${this.parent.labels.materials})` : ""),
      this.parent.labels.keywords.label
    ];
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get _typeCriticalThreshold() {
    return this.parent?.actor?.flags.n5e?.jutsuCriticalThreshold ?? Infinity;
  }

  /* -------------------------------------------- */

  /**
   * The proficiency multiplier for this item.
   * @returns {number}
   */
  get proficiencyMultiplier() {
    return 1;
  }
}