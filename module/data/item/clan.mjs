import SystemDataModel from "../abstract.mjs";
import { AdvancementField, IdentifierField } from "../fields.mjs";
import ItemDescriptionTemplate from "./templates/item-description.mjs";

/**
 * Data definition for Clan items.
 * @mixes ItemDescriptionTemplate
 *
 * @property {string} identifier        Identifier slug for this clan.
 * @property {number} levels            Current number of levels in this clan.
 * @property {object[]} advancement     Advancement objects for this clan.
 * @property {string[]} saves           Savings throws in which this clan grants proficiency.
 * @property {object} skills            Available clan skills and selected skills.
 * @property {number} skills.number     Number of skills selectable by the player.
 * @property {string[]} skills.choices  List of skill keys that are valid to be chosen.
 * @property {string[]} skills.value    List of skill keys the player has chosen.
 */
export default class ClanData extends SystemDataModel.mixin(ItemDescriptionTemplate) {
  /** @inheritdoc */
  static defineSchema() {
    return this.mergeSchema(super.defineSchema(), {
      identifier: new IdentifierField({required: true, label: "N5E.Identifier"}),
      levels: new foundry.data.fields.NumberField({
        required: true, nullable: false, integer: true, min: 0, initial: 1, label: "N5E.ClassLevels"
      }),
      advancement: new foundry.data.fields.ArrayField(new AdvancementField(), {label: "N5E.AdvancementTitle"}),
      saves: new foundry.data.fields.ArrayField(new foundry.data.fields.StringField(), {label: "N5E.ClanSaves"}),
      skills: new foundry.data.fields.SchemaField({
        number: new foundry.data.fields.NumberField({
          required: true, nullable: false, integer: true, min: 0, initial: 2, label: "N5E.ClanSkillsNumber"
        }),
        choices: new foundry.data.fields.ArrayField(
          new foundry.data.fields.StringField(), {label: "N5E.ClanSkillsEligible"}
        ),
        value: new foundry.data.fields.ArrayField(
          new foundry.data.fields.StringField(), {label: "N5E.ClanSkillsChosen"}
        )
      })
    });
  }
}