import BackgroundData from "./background.mjs";
import ClanData from "./clan.mjs";
import ClassData from "./class.mjs";
import ClassModData from "./classmod.mjs";
import ConsumableData from "./consumable.mjs";
import ContainerData from "./container.mjs";
import EquipmentData from "./equipment.mjs";
import FeatData from "./feat.mjs";
import LootData from "./loot.mjs";
import JutsuData from "./jutsu.mjs";
import SubclassData from "./subclass.mjs";
import ToolData from "./tool.mjs";
import WeaponData from "./weapon.mjs";

export {
  BackgroundData,
  ClanData,
  ClassData,
  ClassModData,
  ConsumableData,
  ContainerData,
  EquipmentData,
  FeatData,
  LootData,
  JutsuData,
  SubclassData,
  ToolData,
  WeaponData
};
export {default as ActionTemplate} from "./templates/action.mjs";
export {default as ActivatedEffectTemplate} from "./templates/activated-effect.mjs";
export {default as EquippableItemTemplate} from "./templates/equippable-item.mjs";
export {default as ItemDescriptionTemplate} from "./templates/item-description.mjs";
export {default as MountableTemplate} from "./templates/mountable.mjs";
export {default as PhysicalItemTemplate} from "./templates/physical-item.mjs";

export const config = {
  background: BackgroundData,
  container: ContainerData,
  clan: ClanData,
  class: ClassData,
  classmod: ClassModData,
  consumable: ConsumableData,
  equipment: EquipmentData,
  feat: FeatData,
  loot: LootData,
  jutsu: JutsuData,
  subclass: SubclassData,
  tool: ToolData,
  weapon: WeaponData
};